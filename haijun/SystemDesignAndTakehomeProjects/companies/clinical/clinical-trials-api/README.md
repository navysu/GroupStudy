# SemanticBits Java coding challenge

This repository contains bootstrap code for SemanticBits Java coding exercise meant for Java/JEE engineer candidates. The bootstrap code is built as SpringBoot REST API, it also contains a script for loading test data and a Docker container for Postgres DB with sample data pre-loaded.

The 'clinical-trials-api' REST API intends to implement CRUD operation on the ClinicalTrial resource. A ClinicalTrial resource abstracts information about clinical trials managed by NCI. For this exercise the abstraction captures only very minimal trial information namely nct_id, trial title, trial phase and trial completion date.

The Spring Boot bootstrap API implements only the list clinical trials operation using hard coded Clinical Trials data. The bootstrap implementation is meant to provide  boilerplate/bootstrap code for implementing a full REST API, so that the coding exercise can focus on the functionality implementation. 

Candidate is expected to implement additional CRUD operations, test, integrate with a backend DB and/or build a UI for consuming the API on the direction of the SemanticBits interviewer. 

## Instructions 

Instructions for utilizing the bootstrap code are given below.

Clone the repository: `git clone https://github.com/semanticbits/java-coding-challenge.git`

`clinical-trials-api` folder contains the Spring Boot Rest API bootstrap implementation. The application has partial implementation of a hypothetical clinical-trials REST API.

`data` folder has a SQL script for creating a clinical_trial table and populating it with a bunch of clinical trial records. Candidate may run the Sql on a Postgres DB they have locally. If Postgres DB is not already setup locally, use the docker-compose file to bring up a Postgres DB with clinical trial data pre-loaded. 

## Running the clinical trial bootstrap application 

### Pre-requisite 

* Maven 3
* Java 8 or above
* Docker Compose (needed only if you don't have PostgreSQL installed locally)

### Building the app

    cd clinical-trials-api
    mvn package

### Running the application

    cd clinical-trials-api
    java -jar target/clinical-trial-api-0.1.0.jar

### Accessing the API
Once the application is running, the following end points should work

    http://localhost:8080 - Application health check endpoint
    http://localhost:8080/clinicaltrial - Returns a list of Clinical trials

### Setting up the DB 

If you have a local PostgreSQL DB already running, run the following command to create the clinical_trials table and load sample data into an existing local DB.

    psql -U <username> -d <dbname> -f ./data/data.sql

If you do not have a local PostgreSQL db, run the following docker command to start up a PostgreSQL instance with the sample data pre-loaded 

    cd data 
    docker-compose up

This should bring up a PostgreSQL DB on port 5432 with user name 'postgres' and password set to 'password'. 'clinical_trial' table on the public schema on the postgres DB should have the sample data. 

 
Thank you for your interest in working at SemanticBits!
