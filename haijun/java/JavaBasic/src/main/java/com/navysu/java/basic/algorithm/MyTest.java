package com.navysu.java.basic.algorithm;

import java.util.HashSet;
import java.util.Set;

public class MyTest {

	public static void main(String[] args) {
		char c = (char) ('0' + ((Character.getNumericValue('5') + 9) % 10));
		System.out.println(c); 
		assert countBitOnes(1) == 1;
		assert countBitOnes(2) == 1;
		assert countBitOnes(3) == 2;
		assert findMissedNum(new int[] {1, 2, 3, 4,3,1,2})  == 4;
		assert findMissedNum2(new int[] {1, 2, 3, 4,3,1,2})  == 4;
		findMissedTwoNum(new int[] {1, 2, 3, 4,8,3,1,2});
		findMissedNum(new int[] {1, 2, 3, 4,8,3,1,2});
		
	}
	
	private static int countBitOnes(int num) {
		int count  = 0;
		while (num != 0) {
			if ((num & 1) == 1) {
				count++;
			}
			num >>= 1;
		}
		return count;
	}
	
	private static int findMissedNum(int[] nums) {
		Set<Integer> helpSet = new HashSet<Integer>();
		for (int num: nums) {
			if (helpSet.contains(num)) {
				helpSet.remove(num);
			} else {
				helpSet.add(num);
			}
		}
		System.out.println("missed number size: " + helpSet.size());
		return helpSet.iterator().next();
	}

	private static int findMissedNum2(int[] nums) {
		int missNum = 0;
		for (int num: nums) {
			missNum ^= num;
		}
		return missNum;
	}
	
	private static int[] findMissedTwoNum(int[] nums) {
		int missNum1 = 0;
		int missNum2 = 0;
		int missXORnum = 0;
		for (int num: nums) {
			missXORnum ^= num;
		}
		// find the most right 1
		int right1 = 0;
		while (missXORnum > 0) {
			right1++;
			if (((missXORnum >> 1) & 1) == 1) {
				break;
			}
			missXORnum >>= 1;
		}
		
		for (int num: nums) {
			if (((num >> right1) & 1) == 1) {
				missNum1 ^= num;
			} else {
				missNum2 ^= num;
			}
		}
		System.out.println(missNum1 + "/" + missNum2);
		return new int[] {missNum1, missNum2};
	}
}
