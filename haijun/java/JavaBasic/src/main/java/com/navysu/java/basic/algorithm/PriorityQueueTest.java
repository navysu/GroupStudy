package com.navysu.java.basic.algorithm;

import java.util.Comparator;
import java.util.PriorityQueue;

public class PriorityQueueTest {


	public static void main(String[] args) {
		testPriorityQueue();
	}

	public static void testPriorityQueue() {
		PriorityQueue<Node> myQueue = new PriorityQueue<>(new Comparator<Node>() {

			@Override
			public int compare(Node o1, Node o2) {
				// TODO Auto-generated method stub
				return o1.value - o2.value;
			}

		});
		Node node0 = new Node(0, 0);
		Node node1 = new Node(1, 1);
		myQueue.offer(node0);
		myQueue.offer(node1);
		System.out.println(myQueue.peek());
		node0 =  myQueue.poll();
		node0.value = 2;
		myQueue.offer(node0);
		System.out.println(myQueue.peek());
		
	}

}
class Node {
	public int key;
	public int value;

	public Node(int key, int value) {
		this.key = key;
		this.value = value;
	}
	
	@Override
	public String toString() {
		return "key(" + key + ") = " + value;
	}
}
