package com.navysu.java.basic.algorithm;

import java.util.stream.Collectors;
import java.util.stream.Stream;


public class SteamExamples {

	public void sortedList() {
		Stream<String> stream = Stream.of("Geeks", "For", "GeeksForGeeks", "A", "Computer", "Portal");

// Since the stream is not being consumed
// this will not throw any exception

// Print the stream
		System.out.println(stream.filter(s -> s.startsWith("G")).peek(s -> System.out.println("Filtered value: " + s))
				.map(String::toUpperCase).peek(s -> System.out.println("Uppercase value :" + s)).count());

		stream = Stream.of("Geeks", "For", "GeeksForGeeks", "A", "Computer", "Portal");

		System.out.print(stream.sorted().collect(Collectors.toList()));

	}

}
