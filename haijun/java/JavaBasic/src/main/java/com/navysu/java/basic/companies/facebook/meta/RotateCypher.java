package com.navysu.java.basic.companies.facebook.meta;

class RotateCypher {

	// Add any helper functions you may need here

	String rotationalCipher(String input, int rotationFactor) {
		// Write your code here
		char[] strArray = input.toCharArray();
		for (int i = 0; i < strArray.length; i++) {
			int val = strArray[i]; // A-Za-z = 10 - 35
			if (val >= '0' && val <= '9') { // less than A a
				strArray[i] = (char) ('0' + (val - '0' + rotationFactor) % 10);
			} else if (val >= 'a' && val <= 'z') {
				strArray[i] = (char) ('a' + (val - 'a' + rotationFactor) % 26);
			} else if (val >= 'A' && val <= 'Z') {
				strArray[i] = (char) ('A' + (val - 'A' + rotationFactor) % 26);
			}
		}
		return String.valueOf(strArray);
	}

	// These are the tests we use to determine if the solution is correct.
	// You can add your own at the bottom.
	int test_case_number = 1;

	void check(String expected, String output) {
		boolean result = (expected.equals(output));
		String rightTick = "ok>";
		String wrongTick = "worng>";
		if (result) {
			System.out.println(rightTick + " Test #" + test_case_number);
		} else {
			System.out.print(wrongTick + " Test #" + test_case_number + ": Expected ");
			printString(expected);
			System.out.print(" Your output: ");
			printString(output);
			System.out.println();
		}
		test_case_number++;
	}

	void printString(String str) {
		System.out.print("[\"" + str + "\"]");
	}

	public static void  main(String[] args) {
		RotateCypher rc = new RotateCypher();
		String input_1 = "All-convoYs-9-be:Alert1.";
		int rotationFactor_1 = 4;
		String expected_1 = "Epp-gsrzsCw-3-fi:Epivx5.";
		String output_1 = rc.rotationalCipher(input_1, rotationFactor_1);
		rc.check(expected_1, output_1);

		String input_2 = "abcdZXYzxy-999.@";
		int rotationFactor_2 = 200;
		String expected_2 = "stuvRPQrpq-999.@";
		String output_2 = rc.rotationalCipher(input_2, rotationFactor_2);
		rc.check(expected_2, output_2);

		// Add your own test cases here

	}
}