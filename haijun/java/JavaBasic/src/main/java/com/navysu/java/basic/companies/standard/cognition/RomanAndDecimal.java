package com.navysu.java.basic.companies.standard.cognition;

import java.util.HashMap;
import java.util.Map;

/**
 * https://literacy.kent.edu/Minigrants/Cinci/romanchart.htm
 * https://leetcode.com/problems/roman-to-integer/
 * 
 * @author navy
 *
 */
public class RomanAndDecimal {
	private static String[] Xs = new String[] { "", "X", "XX", "XXX", "XXXX" };

	private static String[] Ones = new String[] { "", "I", "II", "III" };
	
    public int romanToInt(String s) {
        /*
        IV = 4
        IX = 9
        XL = 40
        XC = 90
        CD = 400
        CM = 900
        If previous value is less than the following value, then the following value minus the previous value
        */
        int num = 0;
        Map<Character, Integer> symbolValueMap = buildSymbolValues();
        for (int i = 0; i < s.length(); i++) {
            int curr = symbolValueMap.get(s.charAt(i));
            if (i < s.length() -1 && curr < symbolValueMap.get(s.charAt(i + 1))) {
                int next = symbolValueMap.get(s.charAt(i + 1));
                num += next - curr;
                ++i;
            } else {
                num += curr;
            } 
        }
        return num;
        
    }
    
    private Map<Character, Integer> buildSymbolValues() {
        Map<Character, Integer> map = new HashMap<>();
        map.put('I', 1);
        map.put('V', 5);
        map.put('X', 10);
        map.put('L', 50);
        map.put('C', 100);
        map.put('D', 500);
        map.put('M', 1000);
        return map;
    }

	public int roman2Decimal(String roman) {
		char[] romanChars = roman.toCharArray();
		int value = 0;
		for (int i = 0; i < romanChars.length; i++) {
			char curr = romanChars[i];
			switch (curr) {
			case 'I':
				if (i < romanChars.length - 1 && isSubtract(curr, romanChars[i + 1])) {
					++i;
					if (romanChars[i] == 'X') {
						value += 9;
					} else {
						value += 4;
					}
				} else {
					value += 1;
				}
				break;
			case 'X':
				if (i < romanChars.length - 1 && isSubtract(curr, romanChars[i + 1])) {
					++i;
					if (romanChars[i] == 'L') {
						value += 40;
					} else {
						value += 90;
					}
				} else {
					value += 10;
				}
				break;
			case 'V':
				value += 5;
				break;
			case 'L':
				value += 50;
				break;
			case 'C':
				value += 100;
				break;
			}
		}
		return value;
	}

	private boolean isSubtract(char current, char after) {
		switch (current) {
		case 'I':
			return after == 'X' || after == 'V';
		case 'X':
			return after == 'L' || after == 'C';
		}
		return false;
	}

	private String handle40(int decimal, int low, int charValue, char ch) {
		StringBuilder strbld = new StringBuilder();
		if (decimal >= low) {
			if (decimal >= charValue) {
				strbld.append(ch);
				strbld.append(decimal2Roman(decimal - charValue));
			} else {
				strbld.append("X").append(ch);
				strbld.append(decimal2Roman(decimal - low));
			}
		}
		return strbld.toString();
	}

	public String decimal2Roman(int decimal) {
		//
		StringBuilder strbld = new StringBuilder();
		if (decimal >= 90) {
			strbld.append(handle40(decimal, 90, 100, 'C'));
		} else if (decimal >= 40 && decimal < 90) {
			strbld.append(handle40(decimal, 40, 50, 'L'));
		} else if (decimal >= 9 && decimal < 40) {
			int tens = decimal / 10;
			strbld.append(Xs[tens]);
			int remain = decimal % 10;
			if (remain < 9) {
				strbld.append(decimal2Roman(remain));
			} else {
				strbld.append("IX");
			}
		} else {
			if (decimal < 4) {
				strbld.append(Ones[decimal]);
			} else if (decimal == 4) {
				strbld.append("IV");
			} else {
				strbld.append("V");
				strbld.append(decimal2Roman(decimal - 5));
			}
		}
		System.out.println(decimal + " roman: " + strbld.toString());
		return strbld.toString();

	}

}
