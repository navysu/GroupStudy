package com.navysu.superstar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SuperStarApplication {

	public static void main(String[] args) {
		SpringApplication.run(SuperStarApplication.class, args);
	}

}
