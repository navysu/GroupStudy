package com.navysu.superstar.chase;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Chase {
	public int add(int a, int b) {
		return a + b;
	}

	// tea and eat
	public void printGroupAngrams(String[] words) {
		// List<List<>>
	}

	public List<List<String>> groupAnagrams(String[] words) {
		Arrays.sort(words);
		/**
		 * "eat", "tea", "ate" "ate", "eat", "tea" Time O(words.length() *
		 * words.length() * longest word length) 
		 * 
		 * Space O(words.length())
		 */
		Map<String, List<String>> groupMap = new HashMap<>();
		List<String> anagramsWords = new ArrayList<>();
		anagramsWords.add(words[0]);
		groupMap.put(words[0], anagramsWords);
		for (int i = 1; i < words.length; i++) {
			String word = words[i];

			boolean isFoundAnagram = false;
			for (String key : groupMap.keySet()) {
				try {
					if (isAnagram(key, word)) {
						groupMap.get(key).add(word);
						isFoundAnagram = true;
						break;
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (!isFoundAnagram) {
				List<String> newAnagramWords = new ArrayList<>();
				newAnagramWords.add(word);
				groupMap.put(word, newAnagramWords);
			}

		}
		List<List<String>> results = new ArrayList<>();
		for (List<String> anagramsGroup : groupMap.values()) {
			results.add(anagramsGroup);
		}
		return results;
	}

	public boolean isAnagram(String wordA, String wordB) throws Exception {
		if (wordA == null || wordB == null) {
			throw new Exception("Parameters cannot be null.");
		}
		if (wordA.length() != wordB.length()) {
			return false;
		}
		if (wordA.equals(wordB)) {
			return true;
		}
		// O(wordA.length()) time
		// O(wordA.length()) space
		Map<Character, Integer> countMap = new HashMap<>();
		for (int i = 0; i < wordA.length(); i++) {
			char chA = wordA.charAt(i);
			countMap.put(chA, countMap.getOrDefault(chA, 0) + 1);
			char chB = wordB.charAt(i);
			countMap.put(chB, countMap.getOrDefault(chB, 0) - 1);
		}
		for (int val : countMap.values()) {
			if (val != 0) {
				return false;
			}
		}
		return true;
	}

	public boolean isAnagram2(String wordA, String wordB) throws Exception {
		if (wordA == null || wordB == null) {
			throw new Exception("Parameters cannot be null.");
		}
		if (wordA.length() != wordB.length()) {
			return false;
		}
		if (wordA.equals(wordB)) {
			return true;
		}
		// O(wordA.length()) time
		// O(wordA.length()) space
		int sum = 0;
		for (int i = 0; i < wordA.length(); i++) {
			sum += (wordA.charAt(i) - wordB.charAt(i));
		}

		return sum == 0;
	}
}
