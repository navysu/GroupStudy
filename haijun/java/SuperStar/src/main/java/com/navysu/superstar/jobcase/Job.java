package com.navysu.superstar.jobcase;

public class Job {
	private String id;
	private Source source;

	/**
	 * types of sources of jobs
	 */
	public enum Source {
		Jobcase, Google, Indeed, Monster
	}

	public Job(Source source, String id) {
		this.id = id;
		this.source = source;
	}

	public Source getSource() {
		return this.source;
	}

	public String getId() {
		return this.id;
	}
}
