package com.navysu.superstar.jobcase;

import java.util.List;

import com.navysu.superstar.jobcase.Job.Source;

/**
 *
 * A common way that Job Boards get their jobs is to ingest feeds from multiple
 * sources. We want to figure out if two feeds are the same by determining if
 * all jobs with the source Jobcase are in the same order in both feeds. Jobs
 * are considered the same if they have the same source and the same id.
 * 
 * Please write a method areJobcaseJobsEquivalent(List<Job> A, List<Job> B) such
 * that it returns true if all the jobs with the source Jobcase are in the same
 * order in both lists.
 * 
 * - In other words, if all the non-Jobcase jobs are removed from two lists,
 * they should be identical. - No third-party library or "standard" filtering /
 * lambda functions can be used in this exercise. For example:
 * 
 * List_1 = [{Source: Jobcase, id: "123"}, {Source: Google, id: "923"}, {Source:
 * Jobcase, id: "930"}, {Source: Jobcase, id: "2675"}, {Source: Indeed, id:
 * "2189"}];
 * 
 * List_2 = [{Source: Google, id: "578"}, {Source: Jobcase, id: "123"}, {Source:
 * Jobcase, id: "930"}, {Source: Google, id: "1897"}, {Source: Jobcase, id:
 * "2675"}];
 * 
 * List_1 and List_2 are identical here because they both contain {Source:
 * Jobcase, id: “123”}, {Source: Jobcase, id: “930”}, and {Source: Jobcase, id:
 * “2675”} in that order.
 * 
 * List_1 = [{Source: Jobcase, id: "123"}];
 * 
 * List_2 = [{Source: Google, id: "578"}];
 * 
 * The following is the Job class you should be using, feel free to use a
 * similar representation in the language of your choice:
 * 
 */

public class JobCase {

	public int add(int a, int b) {
		return a + b;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public boolean areJobcaseJobsEquivalent(List<Job> A, List<Job> B) {
		// int m = A.size();
		// int n = B.size();
		// O(max(m, n) time
		// O(1) space

		int indexA = 0;
		int indexB = 0;
		while (indexA < A.size() && indexB < B.size()) {
			Job jobA = A.get(indexA); // jobcase 123
			if (jobA.getSource() == Source.Jobcase) {
				boolean isFoundinB = false;
				while (indexB < B.size()) {
					Job jobB = B.get(indexB); // Google 578
					if (jobB.getSource() == Source.Jobcase) {
						if (!jobB.getId().equals(jobA.getId())) {
							return false;
						} else {
							isFoundinB = true;
							++indexB;
							break;
						}
					}
					++indexB;
				}
				if (!isFoundinB) {
					return false;
				}
			}
			++indexA;
		}
		while (indexA < A.size()) {
			Job jobA = A.get(indexA);
			if (jobA.getSource() == Source.Jobcase) {
				return false;
			}
			++indexA;
		}
		while (indexB < B.size()) {
			Job jobB = B.get(indexA);
			if (jobB.getSource() == Source.Jobcase) {
				return false;
			}
			++indexB;
		}
		return true;
	}

}
