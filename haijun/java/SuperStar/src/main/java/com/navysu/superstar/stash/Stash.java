package com.navysu.superstar.stash;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Stash {
	public static void main(String[] argv) {
		String[][] logs1 = new String[][] { { "58523", "user_1", "resource_1" }, { "62314", "user_2", "resource_2" },
				{ "54001", "user_1", "resource_3" }, { "200", "user_6", "resource_5" },
				{ "215", "user_6", "resource_4" }, { "54060", "user_2", "resource_3" },
				{ "53760", "user_3", "resource_3" }, { "58522", "user_22", "resource_1" },
				{ "53651", "user_5", "resource_3" }, { "2", "user_6", "resource_1" }, { "100", "user_6", "resource_6" },
				{ "400", "user_7", "resource_2" }, { "100", "user_8", "resource_6" },
				{ "54359", "user_1", "resource_3" }, };

		String[][] logs2 = new String[][] { { "300", "user_1", "resource_3" }, { "599", "user_1", "resource_3" },
				{ "900", "user_1", "resource_3" }, { "1199", "user_1", "resource_3" },
				{ "1200", "user_1", "resource_3" }, { "1201", "user_1", "resource_3" },
				{ "1202", "user_1", "resource_3" } };

		String[][] logs3 = new String[][] { { "300", "user_10", "resource_5" } };

		Map<String, Duration> durations = userDurations(logs3);
		for (String userid : durations.keySet()) {
			System.out.println(userid + ", " + durations.get(userid));
		}
		ResourceAccess ra = getMostRequestIn5Mins(logs1);
		System.out.println(ra.resourceName + ": " + ra.getMax5MinAccess());
		ra = getMostRequestIn5Mins(logs2);
		System.out.println(ra.resourceName + ": " + ra.getMax5MinAccess());
		ra = getMostRequestIn5Mins(logs3);
		System.out.println(ra.resourceName + ": " + ra.getMax5MinAccess());
	}

	private static Map<String, Duration> userDurations(String[][] logs) {
		// logs =. m x 3
		Map<String, Duration> durationMap = new HashMap<>();
		for (String[] row : logs) {
			Duration duration = durationMap.get(row[1]);
			if (duration == null) {
				duration = new Duration();
			}
			duration.addTime(Integer.valueOf(row[0]));
			durationMap.put(row[1], duration);
		}
		return durationMap;
	}

	private static ResourceAccess getMostRequestIn5Mins(String[][] logs) {

		Map<String, ResourceAccess> accessMap = new HashMap<>();
		for (String[] log : logs) {
			ResourceAccess ra = accessMap.get(log[2]);
			if (ra == null) {
				ra = new ResourceAccess(log[2]);
			}
			ra.addTime(Integer.valueOf(log[0]));
			accessMap.put(log[2], ra);
		}
		ResourceAccess result = null;
		for (ResourceAccess ra : accessMap.values()) {
			if (result == null) {
				result = ra;
			} else {
				if (result.getMax5MinAccess() < ra.getMax5MinAccess()) {
					result = ra;
				}
			}
		}
		return result;

	}

}

class ResourceAccess {
	String resourceName;
	List<Integer> timeAccessList;
	private int max = Integer.MIN_VALUE;

	public ResourceAccess(String resourceName) {
		this.resourceName = resourceName;
		this.timeAccessList = new ArrayList<>();
	}

	public void addTime(int timeValue) {
		timeAccessList.add(timeValue);
	}

	public int getMax5MinAccess() {
		if (max != Integer.MIN_VALUE) {
			return max;
		}
		Collections.sort(timeAccessList);
		int duration5min = 300;
		for (int i = 0; i < timeAccessList.size(); i++) {
			int startTime = timeAccessList.get(i);
			int count = 1;
			for (int j = i + 1; j < timeAccessList.size(); j++) {
				if (timeAccessList.get(j) - startTime <= duration5min) {
					++count;
				} else {
					break;
				}
			}
			max = Math.max(max, count);
		}
		return max;
	}
}

class Duration {
	int start;
	int end;

	public Duration() {
		this.start = Integer.MAX_VALUE;
		this.end = Integer.MIN_VALUE;

	}

	public void addTime(int timeValue) {
		if (timeValue < start) {
			this.start = timeValue;
		}
		if (timeValue > end) {
			this.end = timeValue;
		}
	}

	public String toString() {
		return "[" + start + ", " + end + "]";
	}

}
