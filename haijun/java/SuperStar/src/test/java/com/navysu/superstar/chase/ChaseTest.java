package com.navysu.superstar.chase;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ChaseTest {

	Chase chase;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		chase = new Chase();
	}

	@AfterEach
	void tearDown() throws Exception {
		chase = null;
	}

	@DisplayName("Add method")
	@Test
	void testAdd() {
		assertEquals(5, chase.add(2, 3));
	}

	@DisplayName("Check whether words are anagram?")
	@Test
	void testIsAnagram() throws Exception {
		assertTrue(chase.isAnagram("WORD", "DORW"));
		assertFalse(chase.isAnagram("WORD", "DORR"));
		assertFalse(chase.isAnagram("WORRD", "DORR"));
		assertTrue(chase.isAnagram("WORRD", "DORRW"));
		assertTrue(chase.isAnagram("WORRD", "WORRD"));
		Throwable exception = assertThrows(Exception.class, () -> {
			chase.isAnagram("WORRD", null);
		});
		assertEquals("Parameters cannot be null.", exception.getMessage());
	}

	@DisplayName("Check whether words are anagram(No map version)?")
	@Test
	void testIsAnagram2() throws Exception {
		assertTrue(chase.isAnagram2("WORD", "DORW"));
		assertFalse(chase.isAnagram2("WORD", "DORR"));
		assertFalse(chase.isAnagram2("WORRD", "DORR"));
		assertTrue(chase.isAnagram2("WORRD", "DORRW"));
		assertTrue(chase.isAnagram2("WORRD", "WORRD"));
		Throwable exception = assertThrows(Exception.class, () -> {
			chase.isAnagram("WORRD", null);
		});
		assertEquals("Parameters cannot be null.", exception.getMessage());
	}
	
	
	@DisplayName("Group anagram words")
	@Test
	void testGroupAnagrams() {
		String[] words = new String[] {
				"WORD", "DORW", "WW", "RODW", "OR", "RO"
		};
		assertEquals(3, chase.groupAnagrams(words).size());
	}
}
