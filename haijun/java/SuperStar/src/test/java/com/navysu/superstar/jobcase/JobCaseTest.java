package com.navysu.superstar.jobcase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.navysu.superstar.jobcase.Job.Source;

class JobCaseTest {
	
	JobCase jc;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		jc = new JobCase();
	}

	@AfterEach
	void tearDown() throws Exception {
		jc = null;
	}

	@Test
	void testAdd() {
		assertEquals(5, jc.add(2,  3));
	}
	
	@DisplayName("Are Jobcase Jobs Equivalent?")
	@Test
	void testAreJobcaseJobsEquivalent() {
		List<Job> listA = new ArrayList<>();
		List<Job> listB = new ArrayList<>();
		
		listA.add(new Job(Source.Jobcase, "123"));
		listB.add(new Job(Source.Google, "456"));
		assertFalse(jc.areJobcaseJobsEquivalent(listA, listB));
		listB.add(new Job(Source.Jobcase, "123"));
		assertTrue(jc.areJobcaseJobsEquivalent(listA, listB));
		assertTrue(jc.areJobcaseJobsEquivalent(listB, listA));
	}

}
