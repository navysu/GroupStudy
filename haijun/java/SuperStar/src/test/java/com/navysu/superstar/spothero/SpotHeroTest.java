package com.navysu.superstar.spothero;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


class SpotHeroTest {
	
	SpotHero spothero;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		spothero = new SpotHero();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@DisplayName("Test add function")
	@Test
	void testAdd() {
		assertEquals(3, spothero.add(1, 2));
	}

}
