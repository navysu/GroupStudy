package com.example.demopostgre.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import com.example.demopostgre.model.Book;

// @ExtendWith(SpringExtension.class)
@DataJpaTest
public class BookRepositoryTest {

	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private BookRepository repository;
	
	@Test
	@DisplayName("SHould find no books if repository is empty")
	public void should_find_no_books_if_repository_is_empty() {
		Iterable<Book> books = repository.findAll();
		assertThat(books).isEmpty();
	}
	
	@Test
	@DisplayName("should store a book")
	public void storeBook() {
		Book book = repository.save(new Book("Tut title", "Tut desc", true));
		assertThat(book).hasFieldOrPropertyWithValue("title", "Tut title");
		assertThat(book).hasFieldOrPropertyWithValue("description", "Tut desc");
		assertThat(book).hasFieldOrPropertyWithValue("published", true);
	}

	@Test
	@DisplayName("should find all books")
	public void findAllBooks() {
		Book book1 = new Book("Tut#1", "Desc #1", true);
		entityManager.persist(book1);
		Book book2 = new Book("Tut#2", "Desc #2", false);
		entityManager.persist(book2);
		Book book3 = new Book("Tut#3", "Desc #3", true);
		entityManager.persist(book3);
		Book book4 = new Book("Tut#4", "Desc #4", false);
		entityManager.persist(book4);
		Book book5 = new Book("Tut#5", "Desc #5", true);
		entityManager.persist(book5);
		Iterable<Book> books = repository.findAll();
		assertThat(books).hasSize(5).contains(book1, book2, book3, book4, book5);
		
	}
}
