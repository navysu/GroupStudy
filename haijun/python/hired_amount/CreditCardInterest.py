

def solution(charges, payments, on_days):
    day_charges = {d:v for d, v in charges}
    day_payments = {d:-v for d, v in payments}
    balance = 0
    for i in range(0, on_days, 30):
        balance += periodBalance(day_charges, day_payments, balance, i)
    return balance

def periodBalance(day_charges, day_payments, balance, period):
    day = period * 30 + 1
    insterest = 0
    while (day < 31):
        balance += day_charges.get(day, 0) + day_payments.get(day, 0)
        insterest += balance * 0.35 / 365
        day += 1
    return balance + insterest

if __name__ == "__main__":
    charges = [(1, 500)]
    payments = []
    print(solution(charges, payments, 30))
    charges = [(1, 500), (26, 100)]
    payments = [(16, 200)]
    print(solution(charges, payments, 30))